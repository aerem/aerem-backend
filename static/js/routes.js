'use strict';

/**
 * Route configuration for the LeTaloService module.
 */
angular.module('AeremService').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'static/templates/home.html'
            })
            .state('map', {
                url: '/map',
                templateUrl: 'static/templates/map.html'
            })
            
    }
]);