angular
    .module('AeremService').service('MapService',['$http', function ($http){
        this.getStations = function(){
            $http.get('https://aerem.herokuapp.com/app/v1.0/getlaststats').success(function (data) {
                for (i=0,len = data.length;i<len;i++){
                    if(data[i].aqi<=50){data[i].aqc=0.55}
                    else if(50<data[i].aqi<=100){data[i].aqc=0.67}
                    else{data[i].aqc=0.95};
                }
                $scope.stations = data
                console.log(data)
            });
            /*I'm tired*/
            /*[{
                id:1,
                lat:56.134333333,
                lng:40.356333333,
                qc:0.65,
                temp:-3,
                hum:60,
                co2:460,
                aqi:65,
                pm25:14.0
                },{
                id:2,
                lat:56.120350,
                lng:40.374166,
                qc:0.55,
                temp:-2,
                hum:61,
                co2:420,
                aqi:22,
                pm25:5.0
                },{
                id:3,
                lat:56.138106333,
                lng:40.383646333,
                qc:0.55,
                temp:-2,
                hum:63,
                co2:380,
                aqi:38,
                pm25:8.0
                }
                
            ]*/
        };

        this.getStationData = function(id,dtype){
            var data =[{
                id:1,
                temp:[2,1,0,-1,-3,-4,-1,-1,0,-3],
                hum:[70,67,72,80,83,78,67,62,57,60],
                co2:[442, 402, 363, 363, 455, 458, 376, 370, 364, 366],
                date:[1459515600000,1459526400000, 1459537200000,1459548000000 ,1459558800000, 1459569600000, 1459580400000 ,1459591200000 ,1459602000000,1459612800000]
                
                
                }]
            var rd = []
            if (dtype==undefined){
                return data[id]
            }
            else{
                for (var i = 0; i < data[id].date.length;i++){
                    rd.push({x:new Date(data[id].date[i]),y:data[id][dtype][i]})
                }
                return rd
            }
        }
        this.getStationTempGraph = function(id){
            return [{
                values: this.getStationData(id,"temp"),      //values - represents the array of {x,y} data points
                key: 'Temperature', //key  - the name of the series.
                color: '#F44336'
            }]
        }
    }])