angular.module('AeremService')
    .controller('MapCtrl', ['$scope','$timeout','$compile','$http', 'MapService', 'leafletData', '$sce' , function($scope, $timeout, $compile, $http, MapService, leafletData, $sce){
        


        L.Control.Attribution.prototype.options.position = "bottomleft";
        $scope.map = L.map('map',{ zoomControl: false});


        $scope.getPopup = function(pid){
            return '<div id ="p'+ pid +'" class="station-popup">'+
                '<h4>Station '+(pid+1)+'</h4><td class="popup-text">'+
                '<table>'+
                '<tbody>'+
                '<tr><td>Temperature:</td><td>{{stations['+pid+'].temp}} C&#176</td></tr>'+
                '<tr><td>Humidity:</td><td> {{stations['+pid+'].hum}} %</td></tr>'+
                '<tr><td>Air quality:</td><td>{{stations['+pid+'].aqi}} %</td></tr>'+
                '<tr><td>PM2.5:</td><td>{{stations['+pid+'].pm25}} %</td></tr>'+
                '<tr><td>CO2:</td><td>{{stations['+pid+'].co2}} %</td></tr></tbody></table>'+
                '<div style="text-align: right"><button ng-click="sbctrl('+pid+')" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent popup-btn"><i class="material-icons">add</i> More</button>'+
                '</div></tbody></table></div>'
        }


        $scope.materialOptions = {
            fab: true,
            miniFab: true,
            rippleEffect: true,
            toolTips: false,
            color: 'accent'
        };

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: 'Map data &copy; OpenStreetMap contributors'
        }).addTo($scope.map);


        
        navigator.geolocation.getCurrentPosition(function(position){
                $scope.map.setView([position.coords.latitude,position.coords.longitude],13)
            },
            function(err){
                $scope.map.setView([45.27,36.56],3);
        })


        $scope.materialZoomControl = new L.materialControl.Zoom({position: 'topright',materialOptions:$scope.materialOptions}).addTo($scope.map);
        $scope.materialFullscreen = new L.materialControl.Fullscreen({position: 'topright',materialOptions:$scope.materialOptions}).addTo($scope.map);
        
        
        $scope.stations = [];
        $http.get('https://aerem.herokuapp.com/app/v1.0/getlaststats').success(function (data) {
                for (i=0,len = data.length;i<len;i++){
                    if(data[i].aqi<=50){data[i].aqc=0.55}
                    else if(50<data[i].aqi<=100){data[i].aqc=0.67}
                    else{data[i].aqc=0.95};
                }
                $scope.stations = data
                console.log(data)
            /*what a fucking wrong with you, Heroku?*/
        /*setTimeout(function() {
            $http.get('https://aerem.herokuapp.com/app/v1.0/getlaststats').success(function (data){
                var aqc = 0;
                if(data[0].aqi<=50){aqc=0.55}
                else if(50<data[0].aqi<=100){aqc=0.67}
                else{aqi=0.95};
                if ($scope.stations.length==4){
                    $scope.stations.pop();
                    $scope.stations.push({
                        id:4,
                        lat:56.324258, 
                        lng:44.023777,
                        qc:aqc,
                        temp:data[0].temp,
                        hum:data[0].hum,
                        aqi:data[0].aqi,
                        pm25: data[0].pm25,
                        co2:data[0].co2
                    })
                }
                else{
                    $scope.stations.push({
                        id:4,
                        lat:56.324258, 
                        lng:44.023777,
                        qc:aqc,
                        temp:data[0].temp,
                        hum:data[0].hum,
                        aqi:data[0].aqi,
                        pm25: data[0].pm25,
                        co2:data[0].co2
                })};
            });
        }, 5000);*/


        $scope.stationIcon = L.icon({
            iconUrl: './static/images/marker-fane-icon.png',
            iconRetinaUrl: './static/images/marker-fane-icon@2x.png',
            shadowUrl:'./static/images/marker-fane-shadow.png',
            iconAnchor: [13, 41],
            popupAnchor: [-2, -52]/*,
            shadowSize: [68, 95],
            shadowAnchor: [22, 94]*/
        });


        $scope.stations.forEach(function(item,i,arr){
            L.marker([item.lat,item.lng],{icon:$scope.stationIcon}).bindPopup($scope.getPopup(i)).addTo($scope.map)
        });

        $scope.stationsHeat=[];
        $scope.sidebarHTML='';

        $scope.heatmap = L.TileLayer.webglheatmap({size: 3500, autoresize: true, opacity: 0.9});

        for (var i = 0, len = $scope.stations.length; i<len; i++){
            $scope.stationsHeat.push([$scope.stations[i].lat,$scope.stations[i].lng,$scope.stations[i].aqc])
        };

        $scope.heatmap.addDataPoint($scope.stationsHeat);

        $scope.map.addLayer($scope.heatmap);

        $scope.map.on("popupopen", function () {
            $compile($(".station-popup"))($scope);
            $scope.$apply();
        });

        $scope.sidebar = new L.Control.Sidebar('sidebar', {position: 'left'});

        $scope.map.addControl($scope.sidebar);

        $scope.sbctrl = function(id){
            if ($scope.sidebar.isVisible()){
                $scope.sidebar.hide();
            }
            else{
                $scope.stData = MapService.getStationData(id);
                $scope.sidebarHTML= $sce.trustAsHtml('<h3>Station '+$scope.stData.id+'</h3><h5>Temperature:</h5><div id="TempChart"><svg></svg></div><h5>Humidity:</h5><div id="HumChart"><svg></svg></div><h5>Air quality index:</h5><div id="AQIChart"><svg></svg></div><h5>Carbon dioxid:</h5><div id="COChart"><svg></svg></div>');
                $compile($("#sidebar"))($scope);
                //$scope.$apply()
                nv.addGraph(function() {
                    var chart = nv.models.lineChart()
                                .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
                                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
                                //.transitionDuration(350)  //how fast do you want the lines to transition?
                                .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
                                .showYAxis(true)        //Show the y-axis
                                .showXAxis(true)        //Show the x-axis
                    ;

                    chart.xAxis     //Chart x-axis settings
                      .axisLabel('Date')
                      .tickFormat(function(d) {
                        // I didn't feel like changing all the above date values
                        // so I hack it to make each value fall on a different date
                        return d3.time.format('%d %B %Y %H:%M ')(new Date(d));
                      });

                    chart.yAxis     //Chart y-axis settings
                      .axisLabel('Temperature (C)')
                      .tickFormat(d3.format('r'));

                    /* Done setting the chart up? Time to render it!*/
                    var TempData = MapService.getStationTempGraph(id);   //You need data...

                    d3.select('#TempChart svg')    //Select the <svg> element you want to render the chart in.
                      .datum(TempData)         //Populate the <svg> element with chart data...
                      .call(chart);          //Finally, render the chart!

                    //Update the chart when window resizes.
                    nv.utils.windowResize(function() { chart.update() });
                    return chart;
                });
                $scope.sidebar.show();
            }
        }
         });


    }]);
