import os
import psycopg2
import urlparse
import uuid
import json
from flask import Flask, url_for, render_template, Response, request
from werkzeug.contrib.fixers import ProxyFix
from settings import *
import datetime
import AQICalc

app = Flask(__name__)

# some code from heroku for connection to postgres
urlparse.uses_netloc.append("postgres")
url = urlparse.urlparse(os.environ["DATABASE_URL"])

conn = psycopg2.connect(
    database=url.path[1:],
    user=url.username,
    password=url.password,
    host=url.hostname,
    port=url.port
)
conn.autocommit = True

cur = conn.cursor()


@app.route('/')
def index_page():
    return render_template('index.html')


# Test API function
# ---------------------------------
# Get the latest statistic by station's sensors
@app.route(api_adress + 'getlaststats/', methods=['GET'])
def getstats():
    data = list()
    cur.execute("SELECT station_id FROM stations")
    for i in cur.fetchall():
        cur.execute(
            "SELECT * FROM stations_data WHERE station_id = {0} and time = (SELECT max(time) FROM stations_data WHERE station_id = {1})".format(
                i[0], i[0]))
        for dat in cur.fetchall():
            data.append(
                {'data_id': dat[0], 'station_id': dat[1], 'bat': float(dat[2]), 'temp': float(dat[4]), 'hum': dat[5],
                 'aqi': dat[6],
                 'pm25': dat[7], 'co2': dat[8], 'co': float(dat[9]), 'o3': float(dat[10]), 'no2': float(dat[11])})
        cur.execute("SELECT lat, lng FROM stations WHERE station_id = {0}".format(i[0]))
        for dat in cur.fetchall():
            data[-1]["lat"] = float(dat[0])
            data[-1]["lng"] = float(dat[1])

    return Response(status=200, response=json.dumps(data), mimetype="application/json")


# Get all stations
@app.route(api_adress + 'getstations/', methods=['GET'])
def getstations():
    cur.execute("SELECT  * FROM stations ORDER BY station_id DESC;")
    dat = list()
    for i in cur.fetchall():
        dat.append({'station_id': i[0], 'lat': float(i[2]), 'lng': float(i[3])})
    return Response(response=json.dumps(dat), status=200, mimetype="application/json")


# Create new station
@app.route(api_adress + 'createbase/', methods=['POST'])
def create_base():
    return Response(status=404)


# Create data post from station
@app.route(api_adress + 'adddata/<station_uuid>/', methods=['POST','GET'])
def add_data(station_uuid):
    tmStamp = datetime.datetime.now()
    station_uuid = uuid.UUID(station_uuid)
    if request.method == "GET":
        content = json.loads(request.args.get("d"))
    elif request.method == "POST":
        content = json.loads(request.data)
    cur.execute("SELECT  station_id, uuid FROM stations ORDER BY station_id DESC;")
    uuids = list()
    ids = list()
    for i in cur.fetchall():
        uuids.append(uuid.UUID(i[1]))
        ids.append(i[0])
    if station_uuid in uuids:
        station_id = ids[uuids.index(station_uuid)]
        AQI = AQICalc.getAQI([content[0]['pm2.5'], content[0]['o3'], content[0]['co'],
                              content[0]['no2']], ['pm2.5', 'o3', 'co', 'no2'])
        cur.execute(
            "INSERT INTO stations_data (station_id,battery_vol,time,temperature,humidity,aqi,dust,co2,o3,co,no2) VALUES ({0},{1},CURRENT_TIMESTAMP,{2},{3},{4},{5},{6},{7},{8},{9})".format(
                str(station_id), str(content[0]['bat']), str(content[0]['temp']), str(content[0]['hum']), str(AQI),
                str(content[0]['pm2.5']), str(content[0]['co2']), str(content[0]['o3']), str(content[0]['co']),
                str(content[0]['no2'])))

        return Response(response="*"+str((60 - tmStamp.minute)*60000)+"*", status=200)
    else:
        return Response(status=403)


app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run(debug=True)

