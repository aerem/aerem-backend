def getAQI(Concs, Pollutants):
    response = []
    AQIRange = [0, 50, 100, 150, 200, 300, 400, 500]
    PollutantRange = {"pm2.5":[0.0, 12.0, 35.4, 55.4, 150.4, 250.4, 350.4, 500.0],
                      "o3": [0.0, 54.0, 70.0, 85.0, 105.0, 200.0, 300.0, 400.0],
                      "co": [0.0, 4.4, 9.4, 12.4, 15.4, 30.4, 40.4, 50.4],
                      "no2": [0.0, 53.0, 100.0, 360.0, 649.0, 1249.0, 1649.0, 2049.0]}
    for i in range(len(Pollutants)):
        Cmin = Cmax = Imin = Imax = 0
        for j in PollutantRange[Pollutants[i]]:
            if (Concs[i] >= j) and (Concs[i] < PollutantRange[Pollutants[i]][PollutantRange[Pollutants[i]].index(j) + 1]):
                Cmin = j
                Cmax = PollutantRange[Pollutants[i]][PollutantRange[Pollutants[i]].index(j) + 1]
                Imin = AQIRange[PollutantRange[Pollutants[i]].index(j)]
                Imax = AQIRange[PollutantRange[Pollutants[i]].index(j) + 1]
                break
        I = int((Imax - Imin) / (Cmax - Cmin) * (Concs[i] - Cmin) + Imin)
        response.append(I)
    return max(response)
